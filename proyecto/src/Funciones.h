/*
 * Funciones.h
 *
 *  Created on: 09/03/2015
 *      Author: Miguel Angel Gomez Baena
 */

#include "Trama.h"
#include "PuertoSerie.h"
#include "maestro.h"
#include "esclavo.h"
#define SYN 22
#define T 'T'
#define ENQ 05
#define EOT 04
#define ACK 06
#define NACK 21
#define NT '0'

#include <stdio.h>
#include <conio.h>
#include <string>
#include <iostream>

#ifndef FUNCIONES_H_
#define FUNCIONES_H_

class Funciones {

	public:

		/* DESC:   Constructor
		 * PRE:    -
		 * POST:   Construye una instancia de la clase funciones
		 * PARAM:  -
		 * RET:    -
		 * COMP:   O(1)
		 */
		Funciones();

		/* DESC:   Recoge los datos guardados en una cadena y los disecciona en paquetes de 255 caracteres, los inserta en la trama y los envia
		 * 		   Despues reinicia su contenido a 0 para su posterior reutilizacion
		 * PRE:    Se necesita una trama para su uso, se deben teclear datos de entrada
		 * POST:   Se envian las tramas necesarias para enviar el total de la informacion pasada por teclado
		 * PARAM:  HANDLE PuertoCOM: por el puerto donde enviaremos la informacion
		 * RET:    retorna el total de caracteres de la ultima trama
		 * COMP:   O(n)
		 */
		int efe1(char cadena[501], int i, HANDLE PuertoCOM);

		/* DESC:   Metodo que envia una trama de control al otro ordenador
		 * PRE:    trama definida
		 * POST:   Se envia una trama de control definida
		 * PARAM:  Trama t: trama auxiliar que usaremos para enviar la informacion
		 * PARAM:  Puerto por el que enviaremos la informacion
		 * RET:    -
		 * COMP:   O(1)
		 */
		void efe2(Trama t, HANDLE PuertoCOM);

		/* DESC:   Se inicia desde el metodo efe2. muestra por pantallas las opciones para enviar una trama y recoge la opcion deseada
		 * PRE:    -
		 * POST:   Se selecciona el caracter C de una trama
		 * PARAM:  -
		 * RET:    retorna la opcion deseada por el usuario
		 * COMP:   O(1)
		 */
		int pintarMenuTramaC();

		/* DESC:   Metodo mas importante de la clase.
		 * 		   Este metodo esta siempre refrescandose buscando informacion en el buffer de entrada para procesarla.
		 *
		 * 		   Esta dise�ado para recoger tramas de control, de datos y ficheros.
		 * 		   El metodo usando la informacion de los parametros va cargando caracteres 1 a 1 y procesandolos segun del tipo que sean.
		 * 		   Este metodo es muy importante pues esta dise�ado para poder enviar y recibir informacion de los tipos indicados anteriormente de manera simultanea.
		 *
		 * 		   Aplica a los datos las operaciones determinadas para imprimir por pantalla las tramas recibidas, recibir ficheros e iniciar el protocolo maestro-esclavo
		 *
		 * PRE:    -
		 * POST:   Aplica los operaciones definidas a los datos recibidos
		 * PARAM:  int campo: este valor define de que tipo es el caracter recibido, depiendeo de cuanto valga,
		 * 		   el caracter sera insertado en un sitio u otro dependiendo de la opcion escogida
		 * PARAM:  Trama taux: trama donde se ira insertando la informacion a recibir
		 * PARAM:  HANDLE PuertoCOM: puerto por el que se recibe la informacion
		 * PARAM:  int cont: contador para saber en que posicion insertar un caracter cuando se esta recibiendo una trama de datos.
		 * PARAM:  FILE pFichero: puntero que apunta a donde se guarda la informacion, si es null es por pantalla si es diferente de null se escribe en un fichero
		 * RET:    FILE: retorna el puntero a fichero, se retorna para saber cuando el programa deber cortar la comunicacion y terminar de escribir en el fichero
		 * COMP:   O(n^n)
		 */
		FILE * recibirDatos(int &campo, Trama &taux,HANDLE PuertoCOM, int &cont, FILE *pFichero);

		/* DESC:   borra un caracter de la cadena y de pantalla al pulsar retorno de carro
		 * PRE:    se debe haber pulsado alguna tecla antes para que el metodo tenga efecto
		 * POST:   borra un caracter de la cadena a enviar
		 * PARAM:  int contador: posicion del caracter ultimo que debe borrar
		 * PARAM:  char cadena[]: cadena de donde debe borrar el caracter
		 * RET:    -
		 * COMP:   O(1)
		 */
		void borrar(int &contador, char cadena[501]);

		/* DESC:   enviar los caracteres de una trama de control en orden: SYS, D, C, NT
		 * PRE:    trama definida
		 * POST:   envia al otro ordenador una trama de control
		 * PARAM:  Trama t: trama a enviar
		 * PARAM:  HANDLE PuertoCOM: puerto por el que envia la informacion
		 * RET:    -
		 * COMP:   O(1)
		 */
		void enviarTramaControl(Trama t,HANDLE PuertoCOM);

		/* DESC:   enviar los caracteres de una trama de datos en orden: SYS, D, C, NT, L, datos, BCE
		 * PRE:    trama definida
		 * POST:   envia al otro ordenador una trama de datos de forma ordenada
		 * PARAM:  Trama t: trama a enviar
		 * PARAM:  HANDLE PuertoCOM: puerto por el que envia la informacion
		 * PARAM:  char cadena[]: campo datos de la trama a enviar
		 * RET:    -
		 * COMP:   O(1)
		 */
		void enviarTramaDatos(Trama t, char cadena[], HANDLE PuertoCOM);

		/* DESC:   cambia el caracter del campo control depiendiendo del tipo que se le pasa por parametro
		 * PRE:    trama iniciada
		 * POST:   modifica el valor del campo C
		 * PARAM:  trama t: trama a modificar
		 * PARAM:  int tipo: valor del campo a modificar
		 * RET:    -
		 * COMP:   O(1)
		 */
		void asignarTramaControl(Trama &t, int tipo);

		/* DESC:   Inicia una trama de datos para su envio al otro ordenador
		 * PRE:    -
		 * POST:   Reinicia todos los valores o los modifica
		 * PARAM:  Trama t: trama a modificar
		 * PARAM:  char L: tama�o del campo de datos a definir
		 * RET:    -
		 * COMP:   O(1)
		 */
		void asignarTramaDatos(Trama &t, unsigned char L);

		/* DESC:   Metodo que envia un fichero si existe por el puerto al otro ordenador.
		 * 		   Se inicia la aplicacion mandando un caracter '$' por el puerto
		 * 		   Secciona la informacion en tramas de 255 hasta que se acaba el fichero
		 * 		   Posteriormente envia una trama EOT y un caracter '#' para indicar final del fichero
		 * 		   El envio de fichero se puede cancelar pulsando el caracter escape durante la comunicacion
		 *
		 * PRE:    debe existir un fichero a enviar
		 * POST:   el ordenador destino posee una copia de nuestro fichero "fichero.txt"
		 * PARAM:  Trama t: trama auxiliar que usaremos para enviar la informacion
		 * PARAM:  Puerto por el que enviaremos la informacion
		 * RET:    -
		 * COMP:   O(n)
		 */
		void enviarFichero(int &campo, Trama &taux,HANDLE PuertoCOM, int &cont, FILE *pFichero);

		/* DESC:   Metodo que espera caracteres del buffer de entrada y los inserta en una trama dependiendo de su orden de llegada
		 * PRE:    -
		 * POST:   Construye una trama, con todos sus atributos definidos
		 * PARAM:  HANDLE PuertoCOM: puerto por el que se recibe la informacion
		 * RET:    retornada una trama definida
		 * COMP:   O(n)
		 */
		Trama recibirTramaControl(HANDLE PuertoCOM);

		/* DESC:   Este metodo se inicia con la opcion F5
		 * 		   Se inicia el protocolo maestro-esclavo y se pregunta si la estacion que ha pulsado F5 quiere ser maestra o esclava
		 * 		   En caso de ser maestra se le preguntara al usuario si quiere hacer la operacion de sondeo o de seleccion
		 * 		   En caso de ser esclavo se definira esta clase y se esperaran las opciones deseadas para la comunicacion
		 * PRE:    -
		 * POST:   Inicia el protocolo maestro esclavo, manda un caracter al otro ordenador para saber si es maestro o esclavo
		 * PARAM:  HANDLE PuertoCOM: puerto por el que se envia la informacion
		 * RET:    -
		 * COMP:   O(1)
		 */
		void menuMaestro(HANDLE PuertoCOM);

		/* DESC:   Pinta un menu por pantalla con las diferentes opciones del programa
		 * PRE:    -
		 * POST:   Menu mostrado por consola
		 * PARAM:  -
		 * RET:    -
		 * COMP:   O(1)
		 */
		void menu();

		/* DESC:   Destructor
		 * PRE:    -
		 * POST:   Destruye la clase
		 * PARAM:  -
		 * RET:    -
		 * COMP:   O(1)
		 */
		~Funciones();
};
#endif /* FUNCIONES_H_ */


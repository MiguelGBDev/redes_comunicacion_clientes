//============================================================================
// --- SESION1.CPP ---
// Autor :  Miguel Angel Gomez Baena
// Curso : 2014 - 2015
//============================================================================

#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <iostream>
#include "PuertoSerie.h"
#include "Trama.h"
#include "Funciones.h"

#define SYN 22
#define T 'T'
#define ENQ 05
#define EOT 04
#define ACK 06
#define NACK 21
#define STX 2
#define NT '0'

using namespace std;

HANDLE PuertoCOM;

int main() {
	Funciones f;
	char car = 0;
	char cadena[501];
	int i = 0;
	Trama t, taux;
	t.setS(SYN);
	t.setD(T);
	t.setNT(NT);
	int campo = 1;
	int cont = 0;
	FILE *pFichero = NULL;

// Par�metros necesarios al llamar a AbrirPuerto:
// - Nombre del puerto a abrir ("COM1", "COM2", "COM3", ...).
// - Velocidad (1200, 1400, 4800, 9600, 19200, 38400, 57600, 115200).
// - N�mero de bits en cada byte enviado o recibido (4, 5, 6, 7, 8).
// - Paridad (0=sin paridad, 1=impar, 2=par, 3=marca, 4=espacio).
// - Bits de stop (0=1 bit, 1=1.5 bits, 2=2 bits)
	PuertoCOM = AbrirPuerto("COM1", 9600, 8, 0, 0);
	if (PuertoCOM == NULL) {
		printf("Error al abrir el puerto\n");
		getch();
		return (1);
	}

	f.menu();

// Lectura y escritura simult�nea de caracteres:
	while (car != 27 && i < 501) {

		pFichero = f.recibirDatos(campo, taux, PuertoCOM, cont, pFichero);
		if (kbhit()) {
			car = getch();
			if (car != 27) {
				switch (car) {
					case '\0':
						car = getch();
						switch(car){
							case 59:
								cadena[i] = '\0';
								i =  f.efe1(cadena, i, PuertoCOM);
								f.menu();
								break;
							case 60:
								f.efe2(t, PuertoCOM);
								i = 0;
								f.menu();
								break;
							case 61:
								f.enviarFichero(campo, taux,PuertoCOM, cont, pFichero);
								f.menu();
								break;
							case 63:
								f.menuMaestro(PuertoCOM);
								f.menu();
								break;
							}
						break;

					case '\b':
						f.borrar(i, cadena);
						break;

					default:
						if (i < 500) {
							cout << car;
							cadena[i] = car;
							i++;
						}
				}
			}
		}
	}
// Para cerrar el puerto:
	CerrarPuerto(PuertoCOM);

	return 0;
}

/*
 * esclavo.cpp
 *
 *  Created on: 11 de abr. de 2015
 *      Author: Miguel Angel Gomez Baena
 */

#include "esclavo.h"

esclavo::esclavo() {
	// TODO Auto-generated constructor stub
	PuertoCOM = NULL;
}

esclavo::esclavo(HANDLE _PuertoCOM) {
	// TODO Auto-generated constructor stub
	PuertoCOM = _PuertoCOM;
}

void esclavo::sondeo(){

	cout<<endl<<"RECIBIDO: ESCLAVO"<<endl<<"OPERACION SONDEO"<<endl;

	char datos[255];
	boolean ntb = false;
	unsigned char nt = '0';
	unsigned char d = 'T';
	char c = '�', cambiado = 0;

	envio.setS(SYN);
	envio.setD('T');
	envio.setC(ACK);
	envio.setNT(NT);
	enviarTramaControl();

	FILE *puntero;
	puntero = fopen("fichero.txt", "r+b");

	if(puntero != NULL){

		while(!feof(puntero) && c != 27){

			if(kbhit())
				c = getch();

			int i;
			for (i = 0; i < 254 && !feof(puntero); i++)
				datos[i] = fgetc(puntero);

			if(feof(puntero)) {datos[i] = '\0'; i--;}

			if(ntb)	nt = '1';
			else	nt = '0';
			ntb = !ntb;

			envio.setL(i);
			envio.setBce(envio.calcularBce(datos));

			if(c == '\0'){
				c = getch();
				if(c == 64){
					cambiado = datos[0];
					datos[0] = 37;
					cout<<"Error introducido"<<endl;
				}
			}

			asignarTramaDatos((unsigned char)i, nt, d);
			enviarTramaDatos(datos);
			envio.mostrarCompletoControl(1, 0);
			recibirTramaControl(1);

			datos[0] = cambiado;

			if(recibo.getC() != ACK){
				enviarTramaDatos(datos);
				envio.mostrarCompletoControl(1, 0);
				recibirTramaControl(1);
			}
			cambiado = 0;
		}
		fclose(puntero);

		if(c == 27)
			cout<<endl<<"Envio cancelado por el usuario"<<endl;

		envio.setS(SYN);
		envio.setD('T');
		envio.setC(EOT);
		ntb = 0;
		nt = '0';
		envio.setNT(nt);
		enviarTramaControl();
		cout<<endl<<"Envio finalizado, esperando para cortar la comunicacion..."<<endl<<endl;
		recibirTramaControl(1);
		ntb = !ntb;

		while(recibo.getC() != ACK){
			if(ntb)	nt = '1';
			else	nt = '0';
			ntb = !ntb;
			envio.setNT(nt);
			enviarTramaControl();
			recibirTramaControl(1);
		}
	}
	else{
		envio.setS(SYN);
		envio.setD('T');
		envio.setC(EOT);
		envio.setNT('0');
		enviarTramaControl();
		cout<<endl<<"ERROR: No existe ningun fichero a enviar"<<endl;
	}

	cout<<endl<<"Terminada la operacion de SONDEO"<<endl<<endl;
}

void esclavo::seleccion(){

	cout<<endl<<"RECIBIDO: ESCLAVO"<<endl<<"OPERACION SELECCION"<<endl<<endl;

	unsigned char d = 'R';

	envio.setS(SYN);
	envio.setD(d);
	envio.setC(ACK);
	envio.setNT(NT);

	enviarTramaControl();

	recibirTramaControl(0);

	if(recibo.getC() != EOT){

		recibirTramaDatos();

		if(recibo.calcularBce() != recibo.getBce()){
			cout<<endl<<"Trama erronea"<<endl;
			recibo.mostrarCompletoControl(0, 1);
			envio.setC(NACK);
			envio.setNT(recibo.getNT());
			enviarTramaControl();
			recibirTramaControl(0);
			recibirTramaDatos();
			recibo.mostrarCompletoControl(0, 1);
			envio.setC(ACK);
		}
		else
			recibo.mostrarCompletoControl(0, 0);

		FILE *puntero = fopen("fichero.txt", "w+b");

		while(recibo.getC() != EOT){

			fputs(recibo.Datos, puntero);

			envio.setNT(recibo.getNT());
			enviarTramaControl();
			recibirTramaControl(0);
			if(recibo.getC() == STX)
				recibirTramaDatos();

			if(recibo.calcularBce() != recibo.getBce()){
				cout<<endl<<"Trama erronea"<<endl;
				recibo.mostrarCompletoControl(0, 1);
				envio.setC(NACK);
				envio.setNT(recibo.getNT());
				enviarTramaControl();
				recibirTramaControl(0);
				recibirTramaDatos();
				recibo.mostrarCompletoControl(0, 1);
				envio.setC(ACK);
			}
			else
				recibo.mostrarCompletoControl(0, 0);
		}
		fclose(puntero);

		envio.setS(SYN);
		envio.setD(d);
		envio.setC(ACK);
		envio.setNT(NT);

		enviarTramaControl();
		cout<<endl<<"Se ha recibido el fichero completo"<<endl;
	}
	else
		cout<<endl<<"ERROR: La estacion maestro no posee ningun fichero a enviar"<<endl;

	cout<<endl<<"Terminada la operacion de SELECCION"<<endl<<endl;
}

void esclavo::esperaDatos(){

	recibirTramaControl(1);

	if(recibo.getC() == ENQ){

		if(recibo.getD() == 'R')
			seleccion();
		else if(recibo.getD() == 'T')
			sondeo();
		else
			cout<<"Error de Transmision"<<endl;
	}else
		cout<<"Error de Transmision"<<endl;
}

char esclavo::escRecibirCaracter(HANDLE PuertoCOM){

	char c;

	while(!c)
		c = RecibirCaracter(PuertoCOM);

	return c;
}

void esclavo::recibirTramaControl(bool mostrar){

	 recibo.setS(escRecibirCaracter(PuertoCOM));
	 recibo.setD(escRecibirCaracter(PuertoCOM));
	 recibo.setC(escRecibirCaracter(PuertoCOM));
	 recibo.setNT(escRecibirCaracter(PuertoCOM));
	 if(mostrar)
		 recibo.mostrarCompletoControl( 0, 0);
}

void esclavo::enviarTramaControl() {

	EnviarCaracter(PuertoCOM, envio.getS());
	EnviarCaracter(PuertoCOM, envio.getD());
	EnviarCaracter(PuertoCOM, envio.getC());
	EnviarCaracter(PuertoCOM, envio.getNT());
	envio.mostrarCompletoControl(1, 0);
}

void esclavo::asignarTramaDatos(unsigned char L, unsigned char nt, unsigned char d) {

	envio.setS(SYN);
	envio.setD(d);
	envio.setC(STX);
	envio.setNT(nt);
	envio.setL(L);
}

void esclavo::enviarTramaDatos(char cadena[]){

	EnviarCaracter(PuertoCOM, envio.getS());
	EnviarCaracter(PuertoCOM, envio.getD());
	EnviarCaracter(PuertoCOM, envio.getC());
	EnviarCaracter(PuertoCOM, envio.getNT());
	EnviarCaracter(PuertoCOM, envio.getL());
	EnviarCadena(PuertoCOM, cadena, envio.getL());
	EnviarCaracter(PuertoCOM, envio.getBce());
}

void esclavo::recibirTramaDatos(){

	recibo.setL(escRecibirCaracter(PuertoCOM));
	int i;
	for (i = 0; i < recibo.getL(); i++)
		recibo.setDatos(escRecibirCaracter(PuertoCOM), i);

	recibo.setDatos('\0', i);
	recibo.setBce(escRecibirCaracter(PuertoCOM));
}

esclavo::~esclavo() {
	// TODO Auto-generated destructor stub
}



/*
 * maestro.cpp
 *
 *  Created on: 11 de abr. de 2015
 *      Author: Miguel Angel Gomez Baena
 */

#include "maestro.h"

maestro::maestro() {
	// TODO Auto-generated constructor stub
	PuertoCOM = NULL;
}

maestro::maestro(HANDLE _PuertoCOM) {
	// TODO Auto-generated constructor stub
	PuertoCOM = _PuertoCOM;
}

void maestro::menuOperacion(){

	envio.setS(SYN);
	envio.setC(ENQ);
	envio.setNT(NT);

	cout<<endl<<"ESTACION MAESTRO"<<endl;
	cout<<endl<<"Seleccione tipo de operacion..."<<endl;
	cout<<"Pulse 1 para Seleccion..."<<endl;
	cout<<"Pulse 2 para Sondeo..."<<endl;
	char e = getch();
	cout<<e<<endl;

	if(e == 49){
		envio.setD('R');
		cout<<endl<<"OPERACION SELECCION"<<endl<<endl;
	}else{
		envio.setD('T');
		cout<<endl<<"OPERACION SONDEO"<<endl;
	}

	enviarTramaControl();

	switch(envio.getD()){
		case 'R':
			seleccion();
			break;
		case 'T':
			sondeo();
			break;
		default:
			cout<<"La opcion elegida es incorrecta"<<endl;
	}
}

void maestro::seleccion(){

	char datos[255];
	boolean ntb = false;
	char c = '�', cambiado = 0;

	envio.setS(SYN);
	envio.setC(ENQ);
	envio.setNT(NT);

	unsigned char nt = '0';
	unsigned char d = 'R';

	recibirTramaControl(1);

	FILE *puntero;
	puntero = fopen("fichero.txt", "r+b");

	if(puntero != NULL){

		while(!feof(puntero) && c != 27){

			if(kbhit())
				c = getch();

			int i;
			for (i = 0; i < 254 && !feof(puntero); i++)
				datos[i] = fgetc(puntero);

			if(feof(puntero)) {datos[i] = '\0'; i--;}

			if(ntb)	nt = '1';
			else	nt = '0';
			ntb = !ntb;

			envio.setL(i);
			envio.setBce(envio.calcularBce(datos));

			if(c == '\0'){
				c = getch();
				if(c == 64){
					cambiado = datos[0];
					datos[0] = 37;
					cout<<"Error introducido"<<endl;
				}
			}

			if(c == 27)
				cout<<endl<<"Se ha cancelado el envio del fichero"<<endl<<endl;

			asignarTramaDatos((unsigned char)i, nt, d);
			enviarTramaDatos(datos);
			envio.mostrarCompletoControl(1, 0);
			recibirTramaControl(1);

			datos[0] = cambiado;

			if(recibo.getC() != ACK){
				enviarTramaDatos(datos);
				envio.mostrarCompletoControl(1, 0);
				recibirTramaControl(1);
			}
			cambiado = 0;
		}

		envio.setNT(NT);
		envio.setC(EOT);
		enviarTramaControl();
		recibirTramaControl(1);

		cout<<endl<<"Operacion realizada correctamente"<<endl;
	 }
	 else{
		cout<<endl<<"ERROR: No existe ningun fichero para enviar"<<endl<<endl;
		envio.setS(SYN);
		envio.setC(EOT);
		envio.setNT(NT);
		enviarTramaControl();
	 }

	fclose(puntero);
	cout<<endl<<"Terminada la operacion de SELECCION"<<endl<<endl;
}

void maestro::sondeo(){

	envio.setS(SYN);
	envio.setD('T');
	envio.setC(ACK);

	recibirTramaControl(1);

	recibirTramaControl(0);

	if(recibo.getC() != EOT){

		recibirTramaDatos();

		if(recibo.calcularBce() != recibo.getBce()){
			cout<<endl<<"Trama erronea"<<endl;
			recibo.mostrarCompletoControl(0, 1);
			envio.setC(NACK);
			envio.setNT(recibo.getNT());
			enviarTramaControl();
			recibirTramaControl(0);
			recibirTramaDatos();
			recibo.mostrarCompletoControl(0, 1);
			envio.setC(ACK);
		}
		else
			recibo.mostrarCompletoControl(0, 0);

		FILE *puntero = fopen("fichero.txt", "w+b");

		while(recibo.getC() != EOT){

			fputs(recibo.Datos, puntero);

			envio.setNT(recibo.getNT());
			enviarTramaControl();
			recibirTramaControl(0);
			if(recibo.getC() == STX)
				recibirTramaDatos();

			if(recibo.calcularBce() != recibo.getBce()){
				cout<<endl<<"Trama erronea"<<endl;
				recibo.mostrarCompletoControl(0, 1);
				envio.setC(NACK);
				envio.setNT(recibo.getNT());
				enviarTramaControl();
				recibirTramaControl(0);
				recibirTramaDatos();
				recibo.mostrarCompletoControl(0, 1);
				envio.setC(ACK);
			}
			else
				recibo.mostrarCompletoControl(0, 0);
		}
		fclose(puntero);

		char e = 0;
		cout<<"Se ha recibido el fichero completo"<<endl;
		cout<<"�Desea cortar la comunicacion?  (1 = Si o 2 = No)"<<endl;
		e = getch();
		cout<<e<<endl;

		while(e != 49){

			envio.setC(NACK);
			envio.setNT(recibo.getNT());
			enviarTramaControl();
			recibirTramaControl(1);

			cout<<endl<<"Protocolo finalizado"<<endl;
			cout<<"�Desea cortar la comunicacion?  (1 = Si o 2 = No)"<<endl;
			e = getch();
			cout<<e<<endl;
		}

		envio.setC(ACK);
		envio.setNT(recibo.getNT());
		enviarTramaControl();
	}
	else
		cout<<endl<<"ERROR: La estacion esclavo no posee ningun fichero"<<endl;

	cout<<endl<<"Terminada la operacion de SONDEO"<<endl<<endl;
}

char maestro::maesRecibirCaracter(HANDLE PuertoCOM){

	char c;

	while(!c)
		c = RecibirCaracter(PuertoCOM);

	return c;
}

void maestro::recibirTramaControl(bool mostrar){

	 recibo.setS(maesRecibirCaracter(PuertoCOM));
	 recibo.setD(maesRecibirCaracter(PuertoCOM));
	 recibo.setC(maesRecibirCaracter(PuertoCOM));
	 recibo.setNT(maesRecibirCaracter(PuertoCOM));
	 if(mostrar)
		 recibo.mostrarCompletoControl( 0, 0);
}

void maestro::enviarTramaControl() {

	EnviarCaracter(PuertoCOM, envio.getS());
	EnviarCaracter(PuertoCOM, envio.getD());
	EnviarCaracter(PuertoCOM, envio.getC());
	EnviarCaracter(PuertoCOM, envio.getNT());
	envio.mostrarCompletoControl(1, 0);
}

void maestro::asignarTramaDatos(unsigned char L, unsigned char nt, unsigned char d) {

	envio.setS(SYN);
	envio.setD(d);
	envio.setC(STX);
	envio.setNT(nt);
	envio.setL(L);
}

void maestro::enviarTramaDatos(char cadena[]){

	EnviarCaracter(PuertoCOM, envio.getS());
	EnviarCaracter(PuertoCOM, envio.getD());
	EnviarCaracter(PuertoCOM, envio.getC());
	EnviarCaracter(PuertoCOM, envio.getNT());
	EnviarCaracter(PuertoCOM, envio.getL());
	EnviarCadena(PuertoCOM, cadena, envio.getL());
	EnviarCaracter(PuertoCOM, envio.getBce());
}

void maestro::recibirTramaDatos(){

	recibo.setL(maesRecibirCaracter(PuertoCOM));
	int i;
	for (i = 0; i < recibo.getL(); i++)
		recibo.setDatos(maesRecibirCaracter(PuertoCOM), i);

	recibo.setDatos('\0', i);
	recibo.setBce(maesRecibirCaracter(PuertoCOM));
}

maestro::~maestro() {
	// TODO Auto-generated destructor stub
}


/*
 * Funciones.cpp
 *
 *  Created on: 09/03/2015
 *      Author: Miguel Angel Gomez Baena
 */
#include "Funciones.h"

using namespace std;

Funciones::Funciones() {
	// TODO Auto-generated constructor stub
}

int Funciones::pintarMenuTramaC() {

	int tipo = 0;
	cout <<endl<<"Escoge Trama de control:" << endl;
	cout <<"1.- Trama ENQ" << endl;
	cout <<"2.- Trama EOT" << endl;
	cout <<"3.- Trama ACK" << endl;
	cout <<"4.- Trama NACK"<< endl;
	cin >> tipo;

	if (tipo < 1 || tipo > 4) {
		cout << "Opcion no valida" << endl;
		cin >> tipo;
	}
	return tipo;
}

FILE *  Funciones::recibirDatos(int &campo, Trama &taux, HANDLE PuertoCOM, int &cont, FILE *pFichero) {

	char car = RecibirCaracter(PuertoCOM);

	if (car) {
		switch (campo) {
			case 1:
				switch(car){
					case 22:
						taux.setS(SYN);
						campo++;
						break;
					case '$':
						pFichero = fopen("fichero.txt", "w+b");
						cout<<endl<<"Recibiendo fichero..."<<endl;
						break;
					case '#':
						fclose(pFichero);
						pFichero = NULL;
						cout<<"Fichero recibido!"<<endl;
						menu();
						break;
					case '!':{	// SOY ESCLAVO
						esclavo esc(PuertoCOM);
						esc.esperaDatos();
						menu();
						break;}
					case '?':{	// SOY Maestro
						maestro maes(PuertoCOM);
						maes.menuOperacion();
						menu();
						break;}
					default:
						cout << car;
					}
				break;
			case 2:
				taux.setD(car);
				campo++;
				break;
			case 3:
				taux.setC(car);
				campo++;
				break;
			case 4:
				taux.setNT(car);
				if(taux.getC() != 2){
					campo = 1;
					taux.mostrarControl();
					menu();
				}else
					campo++;
				break;
			case 5:
				taux.setL(car);
				campo++;
				break;
			case 6:
				taux.setDatos(car, cont);
				if(cont < (int)taux.getL() -1)
					cont++;
				else{
					taux.setDatos('\0', cont+1);
					campo++;
					cont = 0;
				}
				break;
			case 7:
				taux.setBce(car);
				if(pFichero)
					fputs(taux.Datos, pFichero);
				else{
					taux.mostrarDatos();
					if(taux.getL() != 254)
						menu();
				}
				campo = 1;
				break;
		}
	}
	return pFichero;
}

void Funciones::borrar(int &contador, char cadena[501]) {

	if (contador > 0) {
		contador--;
		cout << '\b';
		cout << ' ';
		cadena[contador] = ' ';
		cout << '\b';
	}
}

void Funciones::enviarTramaControl(Trama t, HANDLE PuertoCOM) {

	EnviarCaracter(PuertoCOM, t.getS());
	EnviarCaracter(PuertoCOM, t.getD());
	EnviarCaracter(PuertoCOM, t.getC());
	EnviarCaracter(PuertoCOM, t.getNT());
}

void Funciones::enviarTramaDatos(Trama t, char cadena[], HANDLE PuertoCOM){

	EnviarCaracter(PuertoCOM, t.getS());
	EnviarCaracter(PuertoCOM, t.getD());
	EnviarCaracter(PuertoCOM, t.getC());
	EnviarCaracter(PuertoCOM, t.getNT());
	EnviarCaracter(PuertoCOM, t.getL());
	EnviarCadena(PuertoCOM, cadena, t.getL());
	EnviarCaracter(PuertoCOM, t.getBce());
}

void Funciones::asignarTramaControl(Trama &t, int tipo) {

	switch (tipo) {
		case 1:	t.setC(ENQ);  break;
		case 2: t.setC(EOT);  break;
		case 3: t.setC(ACK);  break;
		case 4:	t.setC(NACK); break;
	}
}

void Funciones::asignarTramaDatos(Trama &t, unsigned char L) {

	t.setS(22);
	t.setD(T);
	t.setC(02);
	t.setNT('0');
	t.setL(L);
	t.setBce(1);
}

int Funciones::efe1(char cadena[501], int i, HANDLE PuertoCOM){

	Trama t;

	while (i > 0){
		if (i < 255){
			asignarTramaDatos(t, (unsigned char)i);
			enviarTramaDatos(t, cadena, PuertoCOM);
			i = 0;
			cadena[0] = '\0';
		}
		else{
			char cadenita[255];
			strncpy(cadenita, cadena, 254);
			cadenita[254] = '\0';
			asignarTramaDatos(t, 254);
			enviarTramaDatos(t, cadenita, PuertoCOM);
			i -= 255;
			int j = 0;
			while (cadena[j] != '\0'){
				cadena[j] = cadena[j+255];
				j++;
			}
			cadena[j] = '\0';
		}
	}
	cout << endl;
	return i;
}

void Funciones::efe2(Trama t, HANDLE PuertoCOM){

	asignarTramaControl(t, pintarMenuTramaC());
	enviarTramaControl(t, PuertoCOM);
}

void Funciones::enviarFichero(int &campo, Trama &taux,HANDLE PuertoCOM, int &cont, FILE *pFichero){

	FILE *pEnvio;
    char datos[255];
    Trama t;
    pEnvio = fopen("fichero.txt", "r+b");
    char c = 0;

    cout<<endl<<"ENVIO DE FICHERO"<<endl;
    cout<<"Iniciando la transferencia"<<endl;

    if(pEnvio != NULL){

    	EnviarCaracter(PuertoCOM, '$');

		while(!feof(pEnvio) && c != 27){

			if(kbhit())
				c = getch();

			int i;
			for (i = 0; i < 254 && !feof(pEnvio); i++)
				datos[i] = fgetc(pEnvio);

			if(feof(pEnvio)) i--;

			asignarTramaDatos(t, (unsigned char)i);
			enviarTramaDatos(t, datos, PuertoCOM);
			recibirDatos(campo, taux, PuertoCOM, cont, pFichero);
		}

		if(c != 27)
			cout<<endl<<"Fichero enviado"<<endl;
		else
			cout<<endl<<"El envio del fichero ha sido cancelado por el usuario"<<endl;

		EnviarCaracter(PuertoCOM, '#');
    }
    else
    	cout<<endl<<"ERROR: El fichero no existe"<<endl;

    fclose(pEnvio);
}

void Funciones::menuMaestro(HANDLE PuertoCOM){

	cout<<endl<<"INICIANDO PROTOCOLO"<<endl;
	cout<<"Pulse 1 para ser maestro..."<<endl;
	cout<<"Pulse 2 para ser esclavo..."<<endl;
	char d = getch();
	cout<<d<<endl;

	switch(d){
		case 49:{
			EnviarCaracter(PuertoCOM, '!');
			maestro maes(PuertoCOM);
			maes.menuOperacion();
			break;}
		case 50:{
			EnviarCaracter(PuertoCOM, '?');
			cout<<"ESTACION ESCLAVO"<<endl;
			cout<<"Esperando peticiones..."<<endl<<endl;
			esclavo esc(PuertoCOM);
			esc.esperaDatos();
			break;}
		default:
			cout<<"Opcion incorrecta"<<endl;
    }
}

Trama Funciones::recibirTramaControl(HANDLE PuertoCOM){

    Trama e;
	e.setS(RecibirCaracter(PuertoCOM));
	e.setD(RecibirCaracter(PuertoCOM));
	e.setC(RecibirCaracter(PuertoCOM));
	e.setNT(RecibirCaracter(PuertoCOM));
	return e;
}

void Funciones::menu(){

	cout<<endl<<"   COMUNICADOR POR PUERTO SERIE   "<<endl;
	cout<<"----------------------------------"<<endl;
	cout<<"Teclee mensaje a enviar y pulse F1"<<endl;
	cout<<"Pulse F2 para enviar trama de control"<<endl;
	cout<<"Pulse F3 para enviar Fichero"<<endl;
	cout<<"Pulse F5 para iniciar MAESTRO-ESCLAVO"<<endl<<endl;
}

Funciones::~Funciones() {
	// TODO Auto-generated destructor stub
}

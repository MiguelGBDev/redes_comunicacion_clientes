/*
 * maestro.h
 *
 *  Created on: 11 de abr. de 2015
 *      Author: Miguel Angel Gomez Baena
 */

#ifndef MAESTRO_H_
#define MAESTRO_H_

#include "Trama.h"
#include "PuertoSerie.h"
#include <stdio.h>
#include <conio.h>
#include <string>
#include <iostream>

#define SYN 22
#define T 'T'
#define ENQ 05
#define EOT 04
#define ACK 06
#define NACK 21
#define STX 2
#define NT '0'

class maestro {

	private:

		/* 	Puerto por el que se envia/recibe la informacion		*/
		HANDLE PuertoCOM;

		/* 	Instancia de la clase trama en la que se ira almacenando la informacion que se vaya a enviar		*/
		Trama envio;

		/* 	Instancia de la clase trama en la que se ira acumulando la informacion recibida		*/
		Trama recibo;

	public:

		/* DESC:   Constructor
		* PRE:    -
		* POST:   Construye una instancia de la clase maestro
		* PARAM:  -
		* RET:    -
		* COMP:   O(1)
		 */
		maestro();

		/* DESC:   Constructor parametrizado de la clase maestro
		 * PRE:    -
		 * POST:   Construye una instancia de la clase maestro con un PuertoCOM definido por parametro
		 * PARAM:  HANDLE PuertoCOM: puerto por el que se va a enviar/recibir la informacion
		 * RET:    -
		 * COMP:   O(1)
		 */
		maestro(HANDLE _PuertoCOM);

		/* DESC:   Destructor de la clase maestro
		 * PRE:    -
		 * POST:   Destruida la clase maestro
		 * PARAM:  -
		 * RET:    -
		 * COMP:   O(1)
		 */
		virtual ~maestro();

		/* DESC:   Operacion de sondeo: en este metodo la estacion que lo inicia, el esclavo, debe enviar a un maestro un fichero llamado "fichero.txt", si lo posee.
		 * 		   El programa envia una trama ENQ para iniciar la comunicacion
		 *		   Despues, despieza el empieza a llegar el fichero poco a poco en tramas de datos.
		 *		   Por cada trama de datos recibida, el programa envia una confirmacion al esclavo en modo de trama ack.
		 *		   Se pueden recibir tramas con errores introducidos pulsando la tecla f6. el programa envia una trama NACK si ocurre esto.
		 *		   Finalmente se recibe una trama EOT de final de fichero y se espera una trama ACK para cortar la comunicacion.
		 *		   Al recibir el fichero el maestro preguntara si desea cortar la comunicacion, enviando trama NACK hasta que el usuario desee.
		 * PRE:    la clase debe estar definida, PuertoCOM correcto.
		 * POST:   Inicio de la operacion de sondeo, se pueden obtener errores. la estacion maestra tendra una copia del fichero.txt que posee el esclavo
		 * PARAM:  -
		 * RET:    -
		 * COMP:   O(n^n)
		 */
		void sondeo();

		/* DESC:   Operacion de seleccion: en este metodo la estacion que lo inicia debe enviar a un esclavo un fichero llamado "fichero.txt", si lo posee.
		 * 		   El programa envia una trama ack para indicar que esta disponible para iniciar la comunicacion
		 *		   Despues, despieza el fichero poco a poco en tramas de datos y las va enviando.
		 *		   Por cada trama de datos enviada, el programa espera una confirmacion del esclavo en modo de trama ack.
		 *		   Se pueden enviar tramas con errores introducidos pulsando la tecla f6. el programa envia la trama incorrecta y ha de recibir una trama NACK
		 *		   Pues la estacion maestra se dara cuenta de que la trama introducida es incorrecta. Entonces el esclavo volvera a enviar la trama, esta vez de manera correcta.
		 *		   Finalmente se envia una trama EOT de final de fichero y se espera una trama ACK para cortar la comunicacion. Si llega una trama NACK simplemente espera la correcta.
		 *		   Se puede cancelar la comunicacion pulsando la tecla ESC, pero el esclavo obtendra una copia incompleta del fichero.
		 * PRE:    Debe existir fichero, la clase debe estar definida, PuertoCOM correcto.
		 * POST:   Inicio de la operacion de sondeo, se pueden insertar errores. la estacion maestra tendra una copia del fichero.txt que posee el esclavo
		 * PARAM:  -
		 * RET:    -
		 * COMP:   O(n^n)
		 */
		void seleccion();

		/* DESC:   bucle que espera hasta que se reciben todos los datos de la trama de control, o sea, sus 4 campos
		 * PRE:    Trama creada e iniciada.
		 * POST:   trama recibo rellena de nuevos datos recibidos.
		 * PARAM:  bool mostrar: si esta a 1 la trama recibida se muestra por pantalla si no no.
		 * RET:    -
		 * COMP:   O(n)
		 */
		void recibirTramaControl(bool mostrar);

		/* DESC:   Envia de forma ordenada todos los campos de la trama envio
		 * PRE:    trama definida anteriormente
		 * POST:   Trama enviada al receptor
		 * PARAM:  -
		 * RET:    -
		 * COMP:   O(1)
		 */
		void enviarTramaControl();

		/* DESC:   Se inserta en la trama a enviar los valores que se le pasan por parametro
		 * PRE:    trama envio existente
		 * POST:   Construye una trama con sus valores determinados
		 * PARAM:  Char L: Longitud del campo datos a enviar
		 * PARAM:  char NT: numero de la trama definida a 0 o 1, secuencialmente se turna su valor
		 * PARAM:  char D:
		 * RET:    -
		 * COMP:   O(1)
		 */
		void asignarTramaDatos(unsigned char L, unsigned char nt, unsigned char d);

		/* DESC:   Despues de formar la trama, se envia por el puerto a la estacion maestro.
		 * PRE:    trama iniciada y definida
		 * POST:   Se envia una trama a la estacion maestro.
		 * PARAM:  char cadena[]: caracteres a insertar en la trama datos
		 * RET:    -
		 * COMP:   O(1)
		 */
		void enviarTramaDatos(char cadena[]);

		/* DESC:   Metodo de la estacion maestro para obtener informacion del usuario sobre que proceso desea iniciar, seleccion o sondeo.
		 * PRE:    -
		 * POST:   Se inicia la comunicacion
		 * PARAM:  -
		 * RET:    -
		 * COMP:   O(1)
		 */
		void menuOperacion();

		/* DESC:   Define los campos L, datos y BCE dependiendo de lo que la otra estacion mande por el puerto
		 * PRE:    trama recibo definida
		 * POST:   trama con los datos insertados y todos los campos correctos
		 * PARAM:  -
		 * RET:    -
		 * COMP:   O(n)
		 */
		void recibirTramaDatos();

		/* DESC:   Bucle infinito que no para hasta que se reciben todos los caracteres correctos en la trama
		 * PRE:    -
		 * POST:   se reciben los datos para insertarlos en la trama
		 * PARAM:  HANDLE PuertoCOM: puerto por donde se va a recibir la informacion
		 * RET:    -
		 * COMP:   O(n)
		 */
		char maesRecibirCaracter(HANDLE PuertoCOM);
};

#endif /* MAESTRO_H_ */

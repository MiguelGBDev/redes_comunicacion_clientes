/*
 * esclavo.h
 *
 *  Created on: 11 de abr. de 2015
 *      Author: Miguel Angel Gomez Baena
 */

#ifndef ESCLAVO_H_
#define ESCLAVO_H_

#include "Trama.h"
#include "PuertoSerie.h"
#include <stdio.h>
#include <conio.h>
#include <string>
#include <iostream>

#define SYN 22
#define T 'T'
#define ENQ 05
#define EOT 04
#define ACK 06
#define NACK 21
#define STX 2
#define NT '0'

class esclavo {

	private:
	
		/* 	Puerto por el que se envia/recibe la informacion		*/
		HANDLE PuertoCOM;
		
		/* 	Instancia de la clase trama en la que se ira almacenando la informacion que se vaya a enviar		*/
		Trama envio;
		
		/* 	Instancia de la clase trama en la que se ira acumulando la informacion recibida		*/
		Trama recibo;

	public:
	
		/* DESC:   Constructor
		 * PRE:    -
		 * POST:   Construye una instancia de la clase esclavo
		 * PARAM:  -
		 * RET:    -
		 * COMP:   O(1)
		 */
		esclavo();
		
		/* DESC:   Constructor parametrizado de la clase esclavo
		 * PRE:    -
		 * POST:   Construye una instancia de la clase esclavo con un PuertoCOM definido por parametro
		 * PARAM:  HANDLE PuertoCOM: puerto por el que se va a enviar/recibir la informacion
		 * RET:    -
		 * COMP:   O(1)
		 */
		esclavo(HANDLE _PuertoCOM);
		
		/* DESC:   Destructor de la clase esclavo
		 * PRE:    -
		 * POST:   Destruida la clase esclavo 
		 * PARAM:  -
		 * RET:    -
		 * COMP:   O(1)
		 */
		virtual ~esclavo();

		/* DESC:   Operacion de sondeo: en este metodo la estacion que lo inicia debe enviar a un maestro un fichero llamado "fichero.txt", si lo posee.
		 * 		   El programa envia una trama ack para indicar que esta disponible para iniciar la comunicacion
		 *		   Despues, despieza el fichero poco a poco en tramas de datos y las va enviando. 
		 *		   Por cada trama de datos enviada, el programa espera una confirmacion del maestro en modo de trama ack.
		 *		   Se pueden enviar tramas con errores introducidos pulsando la tecla f6. el programa envia la trama incorrecta y ha de recibir una trama NACK
		 *		   Pues la estacion maestra se dara cuenta de que la trama introducida es incorrecta. Entonces el esclavo volvera a enviar la trama, esta vez de manera correcta.
		 *		   Finalmente se envia una trama EOT de final de fichero y se espera una trama ACK para cortar la comunicacion. Si llega una trama NACK simplemente espera la correcta.
		 * PRE:    Debe existir fichero, la clase debe estar definida, PuertoCOM correcto.
		 * POST:   Inicio de la operacion de sondeo, se pueden insertar errores. la estacion maestra tendra una copia del fichero.txt que posee el esclavo
		 * PARAM:  -
		 * RET:    -
		 * COMP:   O(n^n)
		 */
		void sondeo();
	
		/* DESC:   Operacion de seleccion: en este metodo la estacion que lo inicia envia un fichero a la estacion esclavo. este fichero se almacena con el nombre "fichero.txt".
		 * 		   De la misma manera que en la operacion anterior, el programa puede introducir fallos pulsando F6. El esclavo puede identificar si la trma es correcta o no.
		 *         Calculando el codigo BCE y comparandolo con el que se pasa por la trama enviada. si es incorrecta se envia una trama NACK y el maestro vuelve a enviar la trama incorrecta.
		 * PRE:    Debe existir fichero, la clase debe estar definida, PuertoCOM correcto.
		 * POST:   Inicio d la operacion de seleccion, se pueden insertar errores. la estacion maestra envia una copia de su fichero a la estacion esclavo.
		 * PARAM:  -
		 * RET:    -
		 * COMP:   O(n^n)
		 */	
		void seleccion();
		
		/* DESC:   bucle que espera hasta que se reciben todos los datos de la trama de control, o sea, sus 4 campos
		 * PRE:    Trama creada e iniciada.
		 * POST:   trama recibo rellena de nuevos datos recibidos.
		 * PARAM:  bool mostrar: si esta a 1 la trama recibida se muestra por pantalla si no no.
		 * RET:    -
		 * COMP:   O(n)
		 */	
		void recibirTramaControl(bool mostrar);
	
		/* DESC:   Envia de forma ordenada todos los campos de la trama envio
		 * PRE:    trama definida anteriormente
		 * POST:   Trama enviada al receptor
		 * PARAM:  -
		 * RET:    -
		 * COMP:   O(1)
		 */	
		void enviarTramaControl();
		
		/* DESC:   La estacion ha recibido la orden de que va a ser esclavo durante el protocolo. 
		 *		   El metodo espera a recibir mas informacion para saber si la operacion a realizar va a ser sondeo o seleccion.
		 * PRE:    -
		 * POST:   Se recibe una trama de control y se inicia la operacion de sondeo o seleccion dependiendo de la informacion recibida
		 * PARAM:  -
		 * RET:    -
		 * COMP:   O(1)
		 */	
		void esperaDatos();
		
		/* DESC:   Se inserta en la trama a enviar los valores que se le pasan por parametro
		 * PRE:    trama envio existente
		 * POST:   Construye una trama con sus valores determinados
		 * PARAM:  Char L: Longitud del campo datos a enviar
		 * PARAM:  char NT: numero de la trama definida a 0 o 1, secuencialmente se turna su valor
		 * PARAM:  char D: 
		 * RET:    -
		 * COMP:   O(1)
		 */	
		void asignarTramaDatos(unsigned char L, unsigned char nt, unsigned char d);
		
		/* DESC:   Despues de formar la trama, se envia por el puerto a la estacion maestro.
		 * PRE:    trama iniciada y definida
		 * POST:   Se envia una trama a la estacion maestro.
		 * PARAM:  char cadena[]: caracteres a insertar en la trama datos
		 * RET:    -
		 * COMP:   O(1)
		 */
		void enviarTramaDatos(char cadena[]);
		
		/* DESC:   Define los campos L, datos y BCE dependiendo de lo que la otra estacion mande por el puerto
		 * PRE:    trama recibo definida
		 * POST:   trama con los datos insertados y odos los campos correctos
		 * PARAM:  -
		 * RET:    -
		 * COMP:   O(n)
		 */
		void recibirTramaDatos();
		
		/* DESC:   Bucle infinito que no para hasta que se reciben todos los caracteres correctos en la trama
		 * PRE:    -
		 * POST:   se reciben los datos para insertarlos en la trama
		 * PARAM:  HANDLE PuertoCOM: puerto por donde se va a recibir la informacion
		 * RET:    -
		 * COMP:   O(n)
		 */
		char escRecibirCaracter(HANDLE PuertoCOM);
};

#endif /* ESCLAVO_H_ */

/*
 * Trama.h
 *
 *  Created on: 24/2/2015
 * Autor: Miguel Angel Gomez Baena
 */

#include <iostream>
using namespace std;

#ifndef TRAMA_H_
#define TRAMA_H_

class Trama {

	private:

		/*	Atributo de la clase trama que define el caracter de sincronismo	*/
		unsigned char S;

		/*	Atributo de la clase trama que define el caracter tipo de transmision. R para transimision de sondeo y T para seleccion	*/
		unsigned char D;

		/*	Atributo de la clase trama que define el tipo de trama. puede ser ENQ, EOT, ACK o NACK	*/
		unsigned char C;

		/*	Atributo de la clase trama que define el numero de trama	*/
		unsigned char NT;

		/*	Atributo de la clase trama que define el tama�o de los datos a enviar	*/
		unsigned char L;

		/*	Atributo de la clase trama que aplica la operacion logica XOR al campo datos para comprobar si se han recibido correctamente	*/
		unsigned char BCE;

	public:

		/*	Atributo de la clase trama en el que se almacena la informacion a recibir	*/
		char Datos[255];

	    /* DESC:   Constructor
	     * PRE:    -
	     * POST:   Construye una trama, con todos sus atributos a cero
	     * PARAM:  -
	     * RET:    -
	     * COMP:   O(1)
	     */
		Trama();

	    /* DESC:   Constructor parametrizado
	     * PRE:    -
	     * POST:   Construye una trama con los atributos pasados por parametro
	     * PARAM:  _S sincronismo
	     * PARAM:  _D tipo transmision
	     * PARAM:  _C tipo de trama
	     * PARAM:  _NT numero de trama
	     * RET:    -
	     * COMP:   O(1)
	     */
		Trama(unsigned char _S, unsigned char _D, unsigned char _C, unsigned char _NT);

	    /* DESC:   Muestra por pantalla el tipo de trama recibida
	     * PRE:    Trama definida anteriormente
	     * POST:   Muestra por pantalla el tipo de trama de control
	     * PARAM:  -
	     * RET:    -
	     * COMP:   O(1)
	     */
		void mostrarControl();

	    /* DESC:   Muestra el valor del campo datos, hasta la longitud definida por el caracter L
	     * PRE:    La trama debe estar definida
	     * POST:   Muestra por pantalla el campo datos
	     * PARAM:  -
	     * RET:    -
	     * COMP:   0(1)
	     */
		void mostrarDatos();

	    /* DESC:   Guarda en el campo S el valor del parametro
	     * PRE:    trama creada
	     * POST:   cambia el valor del campo S
	     * PARAM:  caracter S normalmente definido a 22
	     * RET:    -
	     * COMP:   0(1)
	     */
		void setS(unsigned char S);

	    /* DESC:   Retorno del valor del campo S
	     * PRE:    Trama iniciada
	     * POST:   -
	     * PARAM:  -
	     * RET:    valor del campo S
	     * COMP:   0(1)
	     */
		unsigned char getS();

	    /* DESC:   Guarda en el campo D el valor del parametro
	     * PRE:    trama creada
	     * POST:   cambia el valor del campo D
	     * PARAM:  caracter D
	     * RET:    -
	     * COMP:   0(1)
	     */
		void setD(unsigned char S);

	    /* DESC:   Retorno del valor del campo D
	     * PRE:    Trama iniciada
	     * POST:   -
	     * PARAM:  -
	     * RET:    valor del campo D
	     * COMP:   0(1)
	     */
		unsigned char getD();

	    /* DESC:   Guarda en el campo C el valor del parametro
	     * PRE:    trama creada
	     * POST:   cambia el valor del campo C
	     * PARAM:  caracter C
	     * RET:    -
	     * COMP:   0(1)
	     */
		void setC(unsigned char S);

	    /* DESC:   Retorno del valor del campo C
	     * PRE:    Trama iniciada
	     * POST:   -
	     * PARAM:  -
	     * RET:    valor del campo C
	     * COMP:   0(1)
	     */
		unsigned char getC();

	    /* DESC:   Guarda en el campo NT el valor del parametro
	     * PRE:    trama creada
	     * POST:   cambia el valor del campo NT
	     * PARAM:  caracter NT, valores 0 o 1
	     * RET:    -
	     * COMP:   0(1)
	     */
		void setNT(unsigned char S);

	    /* DESC:   Retorno del valor del campo NT
	     * PRE:    Trama iniciada
	     * POST:   -
	     * PARAM:  -
	     * RET:    valor del campo NT
	     * COMP:   0(1)
	     */
		unsigned char getNT();

	    /* DESC:   Guarda en el campo L el valor del parametro
	     * PRE:    trama creada
	     * POST:   cambia el valor del campo L
	     * PARAM:  caracter L
	     * RET:    -
	     * COMP:   0(1)
	     */
		void setL(unsigned char l);

	    /* DESC:   Retorno del valor del campo L
	     * PRE:    Trama iniciada
	     * POST:   -
	     * PARAM:  -
	     * RET:    valor del campo L
	     * COMP:   0(1)
	     */
		unsigned char getL();

	    /* DESC:   Guarda en el campo Datos caracteres de 1 en 1
	     * PRE:    trama creada
	     * POST:   guarda una cadena en  Datos
	     * PARAM:  caracter a guardar
	     * PARAM:  posicion en la que se guarda el caracter
	     * RET:    -
	     * COMP:   0(1)
	     */
		void setDatos(char caracter, int pos);

	    /* DESC:   Guarda en el campo Bce el valor del parametro
	     * PRE:    trama creada
	     * POST:   cambia el valor de bce
	     * PARAM:  caracter a guardar
	     * RET:    -
	     * COMP:   0(1)
	     */
		void setBce(unsigned char bce);

	    /* DESC:   Retorno del valor del campo BCE
	     * PRE:    Trama iniciada
	     * POST:   -
	     * PARAM:  -
	     * RET:    valor del campo BCE
	     * COMP:   0(1)
	     */
		unsigned char getBce();

	    /* DESC:   Metodo usado para mostrar la informacion de una trama en el protocolo maestro esclavo
	     * PRE:    Maestro-esclavo iniciado, trama creada
	     * POST:   Escrito en consola los valores de una trama
	     * PARAM:  bool enviando: 0 para mostrar trama recibida, 1 para mostrar trama enviada
	     * PARAM:  bool error: 0 si no se ha insertado un error para el calculo del BCE, 1 si
	     * RET:    -
	     * COMP:   0(1)
	     */
		void mostrarCompletoControl(bool enviando, bool error);

	    /* DESC:   metodo que aplica la operacion XOR a los datos que se le envian por parametro
	     * PRE:    datos definidos, clase trama creada
	     * POST:   -
	     * PARAM:  char datos[]: array de char a los que se les va a aplicar la operacion
	     * RET:    Retorna el calculo de aplicar la operacion XOR a todos los caracteres de los datos pasados por parametro
	     * COMP:   O(n)
	     */
		unsigned char calcularBce(char datos[]);

	    /* DESC:   metodo que aplica la operacion XOR al campo Datos de la trama
	     * PRE:    Datos definidos, clase trama creada
	     * POST:   -
	     * PARAM:  -
	     * RET:    Retorna el calculo de aplicar la operacion XOR a todos los caracteres de los datos de la trama
	     * COMP:   O(n)
	     */
		unsigned char calcularBce();

	    /* DESC:   Destructor
	     * PRE:    -
	     * POST:   Destruye instancia de la clase trama
	     * PARAM:  -
	     * RET:    -
	     * COMP:   -
	     */
		~Trama();
};

#endif /* TRAMA_H_ */

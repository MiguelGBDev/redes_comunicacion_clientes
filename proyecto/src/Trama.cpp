/*
 * Trama.cpp
 *
 *  Created on: 24/2/2015
 * Autor: Miguel Angel Gomez Baena
 */

#include "trama.h"
#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <iostream>
using namespace std;

Trama::Trama() {

	S = 0;
	D = 0;
	C = 0;
	NT = '0';
	L = 0;
	BCE = 0;
}

Trama::Trama(unsigned char _S, unsigned char _D, unsigned char _C, unsigned char _NT) {

	S  = _S;
	D  = _D;
	C  = _C;
	NT = _NT;
	L = 0;
	BCE = 0;
}

void Trama::setS(unsigned char _S) {

	S = _S;
}

unsigned char Trama::getS() {

	return S;
}

void Trama::setD(unsigned char _D) {

	D = _D;
}

unsigned char Trama::getD() {

	return D;
}

void Trama::setC(unsigned char _C) {

	this->C = _C;
}

unsigned char Trama::getC() {

	return C;
}

void Trama::setNT(unsigned char _NT) {

	this->NT = _NT;
}

unsigned char Trama::getNT() {

	return NT;
}

void Trama::mostrarControl() {

	switch (this->C) {
		case 05:
			cout << "Recibida Trama ENQ" << endl;
			break;
		case 04:
			cout << "Recibida Trama EOT" << endl;
			break;
		case 06:
			cout << "Recibida Trama ACK" << endl;
			break;
		case 21:
			cout << "Recibida Trama NACK" << endl;
			break;
		case 2:
			cout << "Recibida Trama STX" << endl;
			break;
		default:
			cout << "Trama enviada erroneamente" << endl;
	}
}

unsigned char Trama::getBce() {

	return BCE;
}

void Trama::setBce(unsigned char bce) {

	BCE = bce;
}

unsigned char Trama::getL() {
	return L;
}

void Trama::setL(unsigned char l) {
	L = l;
}

void Trama::setDatos(char caracter, int pos){
	Datos[pos] = caracter;
}

void Trama::mostrarDatos() {
	cout<<Datos;
	if(L != 254)
		cout<<endl;
}

void Trama::mostrarCompletoControl(bool enviando, bool error){

	if (enviando)
		cout<<"Enviada trama :   ";
	else
		cout<<"Recibida trama:   ";

	cout<<D<<"   ";
	switch(C){
		case 02: cout<<"STX "; break;
		case 04: cout<<"EOT "; break;
		case 05: cout<<"ENQ "; break;
		case 06: cout<<"ACK "; break;
		case 21: cout<<"NACK"; break;
		default: cout<<"ERROR";
	}
	cout<<"  "<<NT;

	if(C == 02){
		cout<<"   "<<(int)BCE;
		if(error){
			cout<<"   BCE calculado:    "<<(int)calcularBce();
			if((int)calcularBce() == BCE)
				cout<<"   OK"<<endl;
			else cout<<endl;
		}
		else
			cout<<endl;
	}
	else
		cout<<endl;
}

unsigned char Trama::calcularBce(char datos[]){

	unsigned char resultado = NULL;

	for (int i = 0; i < L; i++)
		resultado = datos[i] ^ resultado;

	if(resultado != 0)
			return resultado;
		else
			return 1;
}

unsigned char Trama::calcularBce(){

	unsigned char resultado = NULL;

	for (int i = 0; i < L; i++)
		resultado = Datos[i] ^ resultado;

	if(resultado != 0)
		return resultado;
	else
		return 1;
}

Trama::~Trama() {
	// TODO Auto-generated destructor stub
}

